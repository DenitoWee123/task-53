import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  const productElements = document.querySelectorAll('.product');

  for (const element of productElements) {
    const price = element.querySelector('.price').innerHTML;
    element.setAttribute('data-price', price);
  }
});
